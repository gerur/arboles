var Arbol = require('../models/arbol');
var Especie = require('../models/especie');

exports.arbol_list = function (req, res) {
    Arbol.allArboles(function (err, arboles) {
        res.render('arboles/index', { arbolitos: arboles });
    });
}

exports.arbol_create_get = function (req, res) {
    res.render('arboles/create');
}

exports.arbol_create_post = function (req, res) {
    var arbolito = new Arbol(
        {
            code: req.body.code,
            alcaldia: req.body.alcaldia,
            direccion: req.body.direccion,           
            latitud:req.body.latitud,
            longitud: req.body.longitud,
            emplazamiento: req.body.emplazamiento,
            especie: req.body.especie,
            estado: req.body.estado,
            aAltura: req.body.aAltura,
            aDiametro: req.body.aDiametro            
        }
    );
    console.log("arbol a añadir", arbolito);
    Arbol.add(arbolito);
    res.redirect('/arboles');
}

exports.arbol_update_get = function (req, res) {
    console.log("req.params", req.params)
    Arbol.findById(req.params.id).exec((err, arbolito) => {
        res.render('arboles/update', {
            arbolito
        });
    })
}

exports.arbol_update_post = function (req, res) {
    var update_values = {
        alcaldia: req.body.alcaldia,
        direccion: req.body.direccion,
        latitud:req.body.latitud,
        longitud: req.body.longitud,
        emplazamiento: req.body.emplazamiento,
        especie: req.body.especie,
        estado: req.body.estado,
        aAltura:req.body.aAltura,
        aDiametro:req.body.aDiametro        
    };
    Arbol.findByIdAndUpdate(req.params.id, update_values,
        (err, arbol) => {
            if (err) {
                console.log(err);
                res.render('arboles/update', {
                    errors: err.errors,
                    arbol
                })
            } else {
                res.redirect('/arboles');
                return;
            }
        })
}

exports.arbol_delete_post = function (req, res) {
    Arbol.findByIdAndDelete(req.body.id, (err) => {
        if (err) {
            next(err);
        } else {
            res.redirect('/arboles');
        }
    });
}