var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');

var emplazamientoSchema = new mongoose.Schema({
    code: Number,
    nomEmplazamiento: String    
});

emplazamientoSchema.statics.createInstance = function (code, nomEmplazamiento) {
    return new this({
        code: code,        
        nomEmplazamiento:nomEmplazamiento
    })
};

emplazamientoSchema.methods.toString = function () {
    return 'code: ' + this.code + ' | nombre ' + this.nomEmplazamiento;
};

emplazamientoSchema.statics.allEmplazamientos = function (cb) {
    return this.find({}, cb);
};

emplazamientoSchema.statics.add = function (unEmplazamiento, cb) {
    return this.create(unEmplazamiento, cb);
};


emplazamientoSchema.statics.findByCode = function (aCode, cb) {
    return this.findOne({ code: aCode }, cb);
};

emplazamientoSchema.statics.removeByCode = function (aCode, cb) {
    return this.deleteOne({ code: aCode }, cb);
};

module.exports = mongoose.model('Emplazamiento', emplazamientoSchema);