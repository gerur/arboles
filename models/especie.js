var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');

var especieSchema = new mongoose.Schema({
    code: Number,
    nomEspecie: String,
    nomCientifico: String,
    familiaEspecie: String,
    alturaMaxEspecie: Number,
    diametroDAP: Number,
    formaCopa: String,
    formaFruto: String,
    colorFruto: String,
    colorCortezaExt: String,
    colorCortezaInt: String,
    caracTronco: String,
    colorHoja: String,
    tamAproxHoja: Number,
    formaHoja: String
});

especieSchema.statics.createInstance = function (code, nomEspecie, nomCientifico, familiaEspecie,
     alturaMaxEspecie, diametroDAP, formaCopa, formaFruto, colorFruto, colorCortezaExt, 
     colorCortezaInt, caracTronco, colorHoja, tamAproxHoja, formaHoja) {
    return new this({
        code: Number,
        nomEspecie: String,
        nomCientifico: String,
        familiaEspecie: String,
        alturaMaxEspecie: Number,
        diametroDAP: Number,
        formaCopa: String,
        formaFruto:formaFruto,
        colorFruto:colorFruto,
        colorCortezaExt:colorCortezaExt,
        colorCortezaInt:colorCortezaInt,
        caracTronco:caracTronco,
        colorHoja:colorHoja,
        tamAproxHoja:tamAproxHoja,
        formaHoja:formaHoja
    });
};

especieSchema.methods.toString = function () {
    return 'code: ' + this.code + ' | nombre ' + this.nomEspecie;
};

especieSchema.statics.allEspecies = function (cb) {
    return this.find({}, cb);
};

especieSchema.statics.add = function (unaEspecie, cb) {
    return this.create(unaEspecie, cb);
};

especieSchema.statics.findByCode = function (aCode, cb) {
    return this.findOne({ code: aCode }, cb);
};

especieSchema.statics.removeByCode = function (aCode, cb) {
    return this.deleteOne({ code: aCode }, cb);
};

module.exports = mongoose.model('Especie', especieSchema);