var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');

var estadoSchema = new mongoose.Schema({
    code: Number,
    nomEstado: String,
    descripcion: String    
});

estadoSchema.statics.createInstance = function (code, nomEstado, descripcion) {
    return new this({
        code: code,        
        nomEstado:nomEstado,
        descripcion: descripcion
    })
};

estadoSchema.methods.toString = function () {
    return 'code: ' + this.code + ' | nombre ' + this.nomEstado;
};

estadoSchema.statics.allEstados = function (cb) {
    return this.find({}, cb);
};

estadoSchema.statics.add = function (unEstado, cb) {
    return this.create(unEstado, cb);
};


estadoSchema.statics.findByCode = function (aCode, cb) {
    return this.findOne({ code: aCode }, cb);
};

estadoSchema.statics.removeByCode = function (aCode, cb) {
    return this.deleteOne({ code: aCode }, cb);
};

module.exports = mongoose.model('Estado', estadoSchema);