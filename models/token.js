const mongoose = require('mongoose');

const Schema = mongoose.Schema;

//guarda una referencia con el esquema de usuario
const tokenSchema = new Schema({
  _userId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Usuario' }, 
  token: { type: String, required: true }, 
  createdAt: { type: Date, required: true, default: Date.now, expires: 43200 } //guarda la fecha de creación, y elimina el token cuando expira de acuerdo al tiempo                                                                                                                                                       
});

tokenSchema.statics.findToken = function(token, cb) {
  return this.findOne({token: token}, cb);
};

module.exports = mongoose.model('Token', tokenSchema);