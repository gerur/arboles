var express = require('express');
var router = express.Router();
var arbolController= require('../controllers/arbol');
var especieController= require('../controllers/especie');
var emplazamientoController= require('../controllers/emplazamiento');
var estadoController= require('../controllers/estado');

router.get('/', arbolController.arbol_list);
router.get('/create', arbolController.arbol_create_get);
router.post('/create', arbolController.arbol_create_post);
router.get('/:id/update', arbolController.arbol_update_get);
router.post('/:id/update', arbolController.arbol_update_post);
router.post('/:id/delete', arbolController.arbol_delete_post);
router.get('/listaEspecie', especieController.especie_lista);
router.get('/listaEmplazamiento', emplazamientoController.emplazamiento_lista);
router.get('/listaEstado', estadoController.estado_lista);


module.exports = router;