var express = require('express');
var router = express.Router();
var emplazamientoController= require('../controllers/emplazamiento');

router.get('/', emplazamientoController.emplazamiento_list);
router.get('/create', emplazamientoController.emplazamiento_create_get);
router.post('/create', emplazamientoController.emplazamiento_create_post);
router.get('/:id/update', emplazamientoController.emplazamiento_update_get);
router.post('/:id/update', emplazamientoController.emplazamiento_update_post);
router.post('/:id/delete', emplazamientoController.emplazamiento_delete_post);


module.exports = router;