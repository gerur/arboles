var express = require('express');
var router = express.Router();
var especieController= require('../controllers/especie');

router.get('/', especieController.especie_list);
router.get('/create', especieController.especie_create_get);
router.post('/create', especieController.especie_create_post);
router.get('/:id/update', especieController.especie_update_get);
router.post('/:id/update', especieController.especie_update_post);
router.post('/:id/delete', especieController.especie_delete_post);


module.exports = router;