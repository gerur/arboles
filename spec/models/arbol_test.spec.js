var mongoose = require('mongoose');
var Arbol = require('../../models/arbol');

describe('Testing Arboles', function () {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology:true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function (done) {
        Arbol.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
        })
    });

    describe('Arbol.createInstance', () => {
        it('crea una instancia de Arbol', () => {
            var arbolito = Arbol.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
            expect(arbolito.code).toBe(1);
            expect(arbolito.color).toBe("verde");
            expect(arbolito.modelo).toBe("urbana");
            expect(arbolito.ubicacion[0]).toEqual(-34.5);
            expect(arbolito.ubicacion[1]).toEqual(-54.1);
        });
    });

    describe('Arbol.allArboles', () => {
        it('comienza vacia', (done) => {
            Arbol.allArboles(function (err, arbolitos) {
                expect(arbolitos.length).toBe(0);
                done();
            });
        });
    });

    describe('Arbol.add', () =>{
        it('agrega solo un arbol', (done) => {
            var unArbol = new Arbol({code:1, color:"verde", modelo:"urbana"});
            Arbol.add(unArbol, function(err, newArbol){
                if(err) console.log(err);
                Arbol.allArboles(function(err, arboles){
                    expect(arboles.length).toEqual(1);
                    expect(arboles[0].code).toEqual(unArbol.code);

                    done();
                })
            });
        });
    });

    describe('Arbol.findByCode', () => {
        it('debe devolver el arbol con code 1', (done) => {
            Arbol.allArboles(function(err, arboles){
                expect(arboles.length).toBe(0);

                var unArbol = new Arbol({code:1, color: "verde", modelo: "urbana"});
                Arbol.add(unArbol, function(err, newArbol){
                    if(err) console.log(err);
                    Arbol.findByCode(1, function (error, targetArbol){
                        expect(targetArbol.code).toBe(unArbol.code);
                        expect(targetArbol.color).toBe(unArbol.color);
                        expect(targetArbol.modelo).toBe(unArbol.modelo);

                        done();
                    });
                });
            });
        });
    });
});

//TESTING SIN MONGODB

/* beforeEach(() => {Arbol.allArboles = []; });

describe('Arbol.allArboles', () => {
    it('comienza vacia', () =>{
        expect(Arbol.allArboles.length).toBe(0);
    });
});

describe('Arbol.add', () =>{
    it('agregamos un', () =>{
        expect(Arbol.allArboles.length).toBe(0); 

        var a = new Arbol(1, 'rojo', 'urbana', [19.504412089928223, -99.14672233203898]);
        Arbol.add(a);

        expect(Arbol.allArboles.length).toBe(1);
        expect(Arbol.allArboles[0]).toBe(a);
    });
});

describe('Arbol.remove', () =>{
    it('agregamos un', () =>{
        expect(Arbol.allArboles.length).toBe(0); 

        var a = new Arbol(1, 'rojo', 'urbana', [19.504412089928223, -99.14672233203898]);
        Arbol.add(a);
        expect(Arbol.allArboles.length).toBe(1); 
        Arbol.removeById(1);
        expect(Arbol.allArboles.length).toBe(0); 
    });
});

describe('Arbol.findById', () =>{
    it('debe devolver el arbol con id 1', ()=>{

        expect(Arbol.allArboles.length).toBe(0); 
        var unArbol = new Arbol(1, "verde", "urbana");
        var unArbol2 = new Arbol(2, "rojo", "montaña");
        Arbol.add(unArbol);
        Arbol.add(unArbol2);

        var targetArbol = Arbol.findById(1);
        expect(targetArbol.id).toBe(1);
        expect(targetArbol.color).toBe(unArbol.color);
        expect(targetArbol.modelo).toBe(unArbol.modelo);
    });
}); */

